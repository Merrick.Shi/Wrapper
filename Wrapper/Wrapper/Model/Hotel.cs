﻿
namespace Wrapper.Model
{
    public class HotelInfo
    {
        public int HotelID { get; set; }

        public string HotelName { get; set; }

        public int CityID { get; set; }

        public string HotelDescription { get; set; }
    }
}
